import React, { useState } from 'react';

function Result({data, type}) {
    const [passwordType, setPasswordType] = useState("password");
    const togglePassword = () => {
        if (passwordType === 'password') {
            setPasswordType("text");
            return;
        }

        setPasswordType("password");
    }

    return (
        <div className='card w-50'>
            {type === 'search' && <div className='card-body'>
                <div className="mb-3 d-flex flex-column justify-content-center align-items-center">
                    <h4>Showing data for "{data.search}"</h4>
                    <i>No data.</i>
                </div>
            </div>}

            {type !== 'search' && <div className='card-body'>
                <div className="mb-3 d-flex flex-row justify-content-between align-items-center">
                    <label for="disabledTextInput" className="form-label mb-0">Username</label>
                    <div className='d-flex justify-content-between align-items-center w-75'>
                        <span>:</span>
                        <input type="text" className="ms-4 form-control" disabled value={data.username} />
                    </div>
                </div>
                <div className="mb-3 d-flex flex-row justify-content-between align-items-center">
                    <label for="disabledTextInput" className="form-label mb-0">Email</label>
                    <div className='d-flex justify-content-between align-items-center w-75'>
                        <span>:</span>
                        <input type="text" className="ms-4 form-control" disabled value={data.email} />
                    </div>
                </div>
                <div className="mb-3 d-flex flex-row justify-content-between align-items-center">
                    <label for="disabledTextInput" className="form-label mb-0">Password</label>
                    <div className='d-flex justify-content-start align-items-center w-75'>
                        <span>:</span>
                        <input type={passwordType} id='password-result' className="ms-4 form-control" disabled value={data.password} />
                    </div>
                </div>
                <div className="mb-3 d-flex flex-row justify-content-between align-items-center">
                    <label for="disabledTextInput" className="form-label mb-0"></label>
                    <div className='d-flex justify-content-start align-items-center w-75'>
                        <span>&nbsp;</span>
                        <input type="checkbox" onClick={togglePassword} className="ms-4" />
                        <span className='ms-2'>Show Password</span>
                    </div>
                </div>
            </div>}
        </div>
    )
}

export default Result;